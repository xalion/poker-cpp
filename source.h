//
//  poker.h
//  poker
//
//  Created by Xalion on 03.03.2018.
//  Copyright © 2018 Xalion. All rights reserved.
//
#include <iostream>
#include <vector>

using namespace std;

typedef pair<string, string> card;

//Объявление функций
vector<card> shuffle_for_table();
vector<card> shuffle_for_hand();

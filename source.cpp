//
//  source.cpp
//  poker
//
//  Created by Xalion on 03.03.2018.
//  Copyright © 2018 Xalion. All rights reserved.
//

#include <iostream>
#include <vector>
#include "source.h"

using namespace std;


const string value[13] = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
const string suit[4] = {"\033[1;31m\u2665\033[0m", "\033[1;30m\u2660\033[0m", "\033[1;30m\u2663\033[0m", "\033[1;31m\u2666\033[0m"};

vector<card> shuffle_for_table() {
    srand( static_cast<unsigned int>(time(NULL)) );
    
    card first_card(value[rand() % 13], suit[rand() % 4]);
    card second_card(value[rand() % 13], suit[rand() % 4]);
    card third_card(value[rand() % 13], suit[rand() % 4]);
    card fourth_card(value[rand() % 13], suit[rand() % 4]);
    card fifth_card(value[rand() % 13], suit[rand() % 4]);
    
    vector<card> table = {first_card, second_card, third_card, fourth_card, fifth_card};
    
    cout << "Cards on table: \t";
    
    for (int i = 0; i < table.size(); i++) {
        cout << table[i].first << table[i].second << '\t';
    }
    
    cout << "\n\n";
    
    return table;
}

vector<card> shuffle_for_hand() {
    card first_card(value[rand() % 13], suit[rand() % 4]);
    card second_card(value[rand() % 13], suit[rand() % 4]);
    
    vector<card> hand = {first_card, second_card};
    
    cout << "Cards in your hand: \t";
    
    for (int i = 0; i < hand.size(); i++) {
        cout << hand[i].first << hand[i].second << '\t';
    }
    
    cout << "\n\n";
    
    return hand;
    
}
